FROM openjdk:8-jdk-alpine
ADD /build/libs/budget-viking-1.0.0.jar budgetviking.jar
ENV USERNAME=testUser
ENV PASSWORD=testPassword
ENTRYPOINT ["java", "-jar", "budgetviking.jar"]
