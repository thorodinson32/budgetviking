package com.vikingcorp.budgetviking.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.vikingcorp.budgetviking.model.Bill;
import com.vikingcorp.budgetviking.service.BillService;

@RestController
public class BillController {
	
	private final BillService billService;
	
	@Autowired
	public BillController(BillService billService) {
		this.billService = billService;
	}
	
	@GetMapping("/bill")
	public ResponseEntity<List<Bill>> getBills(){
		List<Bill> bills = billService.findAll();
		ResponseEntity<List<Bill>> responseEntity = new ResponseEntity<>(bills,
				HttpStatus.OK);

		return responseEntity;
	}

	@GetMapping("/bill/{billName}")
	public ResponseEntity<List<Bill>> getBillsByName(@PathVariable("billName") String billName){
		List<Bill> bills = billService.findByBillName(billName);
		ResponseEntity<List<Bill>> responseEntity = new ResponseEntity<>(bills,
				HttpStatus.OK);

		return responseEntity;
	}

	@GetMapping("/bill/remaining")
	public ResponseEntity<List<Bill>> getRemainingBills(){
		ResponseEntity<List<Bill>> responseEntity = new ResponseEntity<>(billService.findRemainingBills(),
				HttpStatus.OK);

		return responseEntity;
	}

	@PostMapping("/bill")
	public ResponseEntity<Bill> createBill(@RequestBody Bill bill){
		Bill savedBill = billService.saveBill(bill);
		ResponseEntity<Bill> responseEntity = new ResponseEntity<>(savedBill,
				HttpStatus.OK);

		return responseEntity;
	}

	@DeleteMapping("/bill/{billName}")
	public ResponseEntity<String> deleteBillByName(@PathVariable("billName") String billName){
		long id = billService.deleteByBillName(billName);
		
		ResponseEntity<String> responseEntity = new ResponseEntity<>(billName + " successfully deleted",
				HttpStatus.OK);
		
		if(id == 0){
			responseEntity = new ResponseEntity<>(billName + " deletion failed",
				HttpStatus.NOT_FOUND);
		}

		return responseEntity;
	}
}
