package com.vikingcorp.budgetviking.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "bill")
public class Bill {

	@Id private String id;

	private String billName;
	private double amount;
	private int dayOfMonthDue;
	
	
	public String getBillName() {
		return billName;
	}
	public void setBillName(String billName) {
		this.billName = billName;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public int getDayOfMonthDue() {
		return dayOfMonthDue;
	}
	public void setDayOfMonthDue(int dayOfMonthDue) {
		this.dayOfMonthDue = dayOfMonthDue;
	}
	
	
}