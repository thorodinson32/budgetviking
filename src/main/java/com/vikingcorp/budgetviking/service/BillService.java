package com.vikingcorp.budgetviking.service;

import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.vikingcorp.budgetviking.model.Bill;
import com.vikingcorp.budgetviking.repository.BillRepository;

@Component
public class BillService {

	private final BillRepository billRepository;
	
	@Autowired
	public BillService(BillRepository billRepository) {
		this.billRepository = billRepository;
	}

	public List<Bill> findAll(){
		return billRepository.findAll();
	}

	public List<Bill> findByBillName(String billName){
		return billRepository.findByBillName(billName);
	}

	public List<Bill> findRemainingBills(){
		List<Bill> allBillsForMonth = billRepository.findAll();
		
		List<Bill> remainingBillsForMonth = allBillsForMonth.stream()
		.filter(Objects::nonNull)
		.filter(bill -> bill.getDayOfMonthDue() >= getDayOfMonth())
		.collect(Collectors.toList());
		
		return remainingBillsForMonth;
	}
	
	public Bill saveBill(Bill bill){
		return billRepository.save(bill);
	}

	public long deleteByBillName(String billName){
		return billRepository.deleteByBillName(billName);
	}

	private int getDayOfMonth(){
		Calendar cal = Calendar.getInstance();
		return cal.get(Calendar.DAY_OF_MONTH);
	}
}