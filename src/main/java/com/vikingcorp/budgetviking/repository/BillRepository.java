package com.vikingcorp.budgetviking.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.vikingcorp.budgetviking.model.Bill;

@Repository
public interface BillRepository extends MongoRepository<Bill, String> {

	List<Bill> findByBillName(String billName);

	long deleteByBillName(String billName);

}