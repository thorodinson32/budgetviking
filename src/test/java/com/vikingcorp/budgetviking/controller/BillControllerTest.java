package com.vikingcorp.budgetviking.controller;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.vikingcorp.budgetviking.model.Bill;
import com.vikingcorp.budgetviking.service.BillService;

@RunWith(SpringRunner.class)
@WebMvcTest(value = BillController.class, secure = false)
public class BillControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private BillService billService;

	@Test
	public void allBillsAreReturned() throws Exception {
		List<Bill> bills = new ArrayList<Bill>();
		Bill bill = new Bill();
		bill.setBillName("Rent");
		bill.setAmount(1000);
		bill.setDayOfMonthDue(1);
		bills.add(bill);

		Mockito.when(
				billService.findAll()).thenReturn(bills);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
				"/bill").accept(
						MediaType.APPLICATION_JSON);

		mockMvc
		.perform(requestBuilder)
		.andExpect(jsonPath("$[0].billName", is("Rent")))
		.andExpect(jsonPath("$[0].amount", is(1000.0)))
		.andExpect(jsonPath("$[0].dayOfMonthDue", is(1)));
	}

	@Test
	public void givenBillNameBillsReturned() throws Exception {
		List<Bill> bills = new ArrayList<Bill>();
		Bill bill = new Bill();
		bill.setBillName("Rent");
		bill.setAmount(1000);
		bill.setDayOfMonthDue(1);
		bills.add(bill);

		Mockito.when(
				billService.findByBillName("Rent")).thenReturn(bills);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
				"/bill/Rent").accept(
						MediaType.APPLICATION_JSON);

		mockMvc
		.perform(requestBuilder)
		.andExpect(jsonPath("$[0].billName", is("Rent")))
		.andExpect(jsonPath("$[0].amount", is(1000.0)))
		.andExpect(jsonPath("$[0].dayOfMonthDue", is(1)));
	}

	@Test
	public void allRemainingBillsReturned() throws Exception {
		List<Bill> bills = new ArrayList<Bill>();
		Bill bill = new Bill();
		bill.setBillName("Rent");
		bill.setAmount(1000);
		bill.setDayOfMonthDue(1);
		bills.add(bill);

		Mockito.when(
				billService.findRemainingBills()).thenReturn(bills);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
				"/bill/remaining");

		mockMvc
		.perform(requestBuilder)
		.andExpect(jsonPath("$[0].billName", is("Rent")))
		.andExpect(jsonPath("$[0].amount", is(1000.0)))
		.andExpect(jsonPath("$[0].dayOfMonthDue", is(1)));
	}

	@Test
	public void givenRequestBodySaveSuccess() throws Exception{
		Bill bill = new Bill();
		bill.setBillName("Rent");
		bill.setAmount(1000);
		bill.setDayOfMonthDue(1);

		String givenPost = "{\"billName\": \"testremain\",\"amount\": 325,\"dayOfMonthDue\": 30}";

		Mockito.when(
				billService.saveBill(Mockito.any(Bill.class))).thenReturn(bill);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
				"/bill").accept(
						MediaType.APPLICATION_JSON).content(givenPost).contentType(MediaType.APPLICATION_JSON);

		mockMvc
		.perform(requestBuilder)
		.andExpect(jsonPath("$.billName", is("Rent")))
		.andExpect(jsonPath("$.amount", is(1000.0)))
		.andExpect(jsonPath("$.dayOfMonthDue", is(1)));

	}

	@Test
	public void givenNameBillDeletedSuccess() throws Exception{
		Mockito.when(
				billService.deleteByBillName("Rent")).thenReturn(1L);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/bill/Rent");

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		assertThat(result.getResponse().getContentAsString(), is("Rent successfully deleted"));
		assertThat(result.getResponse().getStatus(), is(200));

	}

	@Test
	public void givenNameBillDeletedFail() throws Exception{
		Mockito.when(
				billService.deleteByBillName("Rent")).thenReturn(0L);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/bill/Rent");

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		assertThat(result.getResponse().getContentAsString(), is("Rent deletion failed"));
		assertThat(result.getResponse().getStatus(), is(404));

	}
}
