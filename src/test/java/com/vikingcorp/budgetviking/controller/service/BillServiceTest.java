package com.vikingcorp.budgetviking.controller.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import java.util.Arrays;
import java.util.Calendar;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.vikingcorp.budgetviking.model.Bill;
import com.vikingcorp.budgetviking.repository.BillRepository;
import com.vikingcorp.budgetviking.service.BillService;

@RunWith(MockitoJUnitRunner.class)
public class BillServiceTest {
	
	@Mock
	private BillRepository billRepository;
	
	@InjectMocks
	private BillService billService;
	
	@Test
	public void allBillsReturned(){
		Bill bill1 = new Bill();
		bill1.setBillName("Rent");
		Bill bill2 = new Bill();
		bill2.setBillName("Rent");
		Bill[] bills = {bill1, bill2};
		
		Mockito.when(billRepository.findAll()).thenReturn(Arrays.asList(bills));
		
		assertThat(billService.findAll().size(), is(2));
	}
	
	@Test
	public void givenBillNameBillReturned(){
		Bill bill = new Bill();
		bill.setBillName("Rent");
		Bill[] bills = {bill};
		
		Mockito.when(billRepository.findByBillName("Rent")).thenReturn(Arrays.asList(bills));
		
		assertThat(billService.findByBillName("Rent").get(0).getBillName(), is("Rent"));
	}
	
	@Test
	public void allBillsForMonthAfterCurrentDayReturned(){
		Calendar cal = Calendar.getInstance();
		int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
		Bill bill1 = new Bill();
		bill1.setDayOfMonthDue(dayOfMonth);
		Bill bill2 = new Bill();
		bill2.setDayOfMonthDue(dayOfMonth - 1);
		Bill bill3 = new Bill();
		bill3.setDayOfMonthDue(dayOfMonth + 1);
		Bill[] bills = {bill1, bill2, bill3};
		
		Mockito.when(billRepository.findAll()).thenReturn(Arrays.asList(bills));
		
		assertThat(billService.findRemainingBills().size(), is(2));
	}
	
	@Test
	public void givenBillSavedAndReturned(){
		Bill bill = new Bill();
		bill.setBillName("Rent");
		
		Mockito.when(billRepository.save(Mockito.any(Bill.class))).thenReturn(bill);
		
		assertThat(billService.saveBill(bill).getBillName(), is("Rent"));
	}
	
	@Test
	public void givenBillNameDeleteBill(){
		
		Mockito.when(billRepository.deleteByBillName("Rent")).thenReturn(1L);
		
		assertThat(billService.deleteByBillName("Rent"), is(1L));
	}
}
