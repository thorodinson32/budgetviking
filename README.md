# Budget Viking

Budget Viking is Restful Webservice used to store and retrieve various bills in MongoDB. I created this service as a means to enhance my understanding of Restful Webservices, MongoDB, and Docker, while also providing myself a means to track my monthly bills.
